# -*- coding: utf-8 -*-
from django.views.generic import ListView, DetailView, UpdateView, DeleteView
from django.contrib.auth import get_user_model

from dj_auth.views import (ObjectFilterListMixin, ObjectFilterDetailMixin, ObjectFilterUpdateMixin,
                           ObjectFilterDeleteMixin)
from dj_auth.models import ObjectFilter

from .forms import UserUpdateForm


class UserListView(ObjectFilterListMixin, ListView):
    template_name = 'dj_auth/list.html'
    model = get_user_model()


class UserDetailView(ObjectFilterDetailMixin, DetailView):
    template_name = 'dj_auth/detail.html'
    model = get_user_model()


class UserUpdateView(ObjectFilterUpdateMixin, UpdateView):
    template_name = 'dj_auth/form.html'
    model = get_user_model()
    form_class = UserUpdateForm


class UserDeleteView(ObjectFilterDeleteMixin, DeleteView):
    template_name = 'dj_auth/detail.html'
    model = get_user_model()


class ObjectFilterListView(ObjectFilterListMixin, ListView):
    template_name = 'dj_auth/list.html'
    model = ObjectFilter
