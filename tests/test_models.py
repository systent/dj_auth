#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase, override_settings
from django.conf import settings
from django.db import models, connection
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericForeignKey

from dj_auth.models import ObjectFilter
from dj_auth.constants import FILTER_ID, EXCLUDE_ID

from dj_auth.models import ObjectFilterQuerySetMixin


class BasisModelQuerySet(ObjectFilterQuerySetMixin, models.QuerySet):
    pass


class Person(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False)
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL, on_delete=models.CASCADE, null=True, blank=False)

    objects = BasisModelQuerySet.as_manager()

    class Meta:
        app_label = 'tests'


class Document(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False)

    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE, blank=True, null=True)
    object_id = models.PositiveIntegerField(blank=True, null=True)
    content_object = GenericForeignKey("content_type", "object_id")

    objects = BasisModelQuerySet.as_manager()

    class Meta:
        app_label = 'tests'


class Car(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False)
    person = models.ForeignKey(Person, on_delete=models.CASCADE, null=True, blank=False)

    objects = BasisModelQuerySet.as_manager()

    class Meta:
        app_label = 'tests'


@override_settings(INSTALLED_APPS=["django.contrib.auth",
                                   "django.contrib.contenttypes",
                                   "django.contrib.sessions",
                                   "django.contrib.sites",
                                   "dj_auth",
                                   "tests"
                                   ],
                   )


@override_settings(MIDDLEWARE=[
    'django.middleware.security.SecurityMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware'])
class ObjectFilterQuerySetMixinTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super(ObjectFilterQuerySetMixinTest, cls).setUpClass()

        # Create a dummy model
        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(Person)
            schema_editor.create_model(Document)
            schema_editor.create_model(Car)

        ContentType.objects.get_or_create(app_label='tests', model='person')
        ContentType.objects.get_or_create(app_label='tests', model='document')
        ContentType.objects.get_or_create(app_label='tests', model='car')

    def setUp(self):
        self.user = get_user_model().objects.create_user('foo', 'user@test.com', 'bar')
        self.user.is_staff = True
        self.user.save()

        self.group, created = Group.objects.get_or_create(name='New Group')

    def test_user_isnull_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        objectexclude = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[])
        objectexclude.save()
        objectexclude.users.add(self.user)

        self.assertEquals(Person.objects.apply_user_object_filter(user=self.user).count(), 1)

    def test_user_isnull_filter(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        objectexclude = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[])
        objectexclude.save()
        objectexclude.users.add(self.user)

        self.assertEquals(Person.objects.apply_user_object_filter(user=self.user).count(), 1)

    def test_document_user_isnull_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()
        carfoo = Car(name='foo', person=foo)
        carfoo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()
        carbar = Car(name='bar', person=bar)
        carbar.save()

        doc1 = Document(name='foo document', content_type=ContentType.objects.get_for_model(Car),
                        object_id=carfoo.id)
        doc1.save()

        doc2 = Document(name='bar document', content_type=ContentType.objects.get_for_model(Car),
                        object_id=carbar.id)
        doc2.save()

        objectexclude = ObjectFilter(content_type=ContentType.objects.get_for_model(Person),
                                     filter_type=EXCLUDE_ID,
                                     filter_values=[foo.id, ])
        objectexclude.save()
        objectexclude.users.add(self.user)

        self.assertEquals(
            Document.objects.apply_user_generic_objects_filter(user=self.user).count(), 1)

    def test_document_user_isnull_filter(self):
        foo = Person(name='foo', user=None)
        foo.save()
        carfoo = Car(name='foo', person=foo)
        carfoo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()
        carbar = Car(name='bar', person=bar)
        carbar.save()

        doc1 = Document(name='foo document',
                        content_type=ContentType.objects.get_for_model(Car),
                        object_id=carfoo.id)
        doc1.save()

        doc2 = Document(name='bar document',
                        content_type=ContentType.objects.get_for_model(Car),
                        object_id=carbar.id)
        doc2.save()

        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Person),
                                    filter_type=FILTER_ID,
                                    filter_values=[foo.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        self.assertEquals(
            Document.objects.apply_user_generic_objects_filter(user=self.user).count(), 1)

    def test_car_person_filter(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Person),
                                    filter_type=FILTER_ID,
                                    filter_values=[])
        objectfilter.save()
        objectfilter.users.add(self.user)

        self.assertEquals(Car.objects.apply_user_object_filter(user=self.user).count(), 0)

    def test_car_person_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(Person), filter_type=EXCLUDE_ID,
            filter_values=[])
        objectfilter.save()
        objectfilter.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query.children, [('person__isnull', True)])

    def test_car_person_list_filter(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query.children,
                          [('person__user__in', [2, 3]), ('person__isnull', True)])
        self.assertEquals(exclude_query, '')

    def test_car_person_list_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values="[%s, ]" % self.user.id)
        objectfilter.save()
        objectfilter.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query.children, [('person__user__in', [self.user.id])])

    def test_car_person_list_exlcude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query.children, [('person__user__in', [2, 3])])

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'global_related_fields_exclude': (),
                                'related_filter_fields_exclude': {'tests.car': ('person', ), },
                                }
                       )
    def test_related_filter_fields_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query, '')

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': ('person'),
                                'global_related_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_global_fields_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query, '')

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'global_related_fields_exclude': ('user'),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_global_related_fields_exclude_filter(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query, '')

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'global_related_fields_exclude': ('user'),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_global_related_fields_exclude_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        filter_query, exclude_query = ObjectFilter.build_filter(
            user=self.user, content_type=ContentType.objects.get_for_model(Car))
        self.assertEquals(filter_query, '')
        self.assertEquals(exclude_query, '')


@override_settings(INSTALLED_APPS=["django.contrib.auth",
                                   "django.contrib.contenttypes",
                                   "django.contrib.sites",
                                   "dj_auth",
                                   "tests"
                                   ],
                   )
class ObjectFilterQuerySetMixinWithoutSessionTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super(ObjectFilterQuerySetMixinWithoutSessionTest, cls).setUpClass()

        # Create a dummy model
        with connection.schema_editor() as schema_editor:
            schema_editor.create_model(Person)
            schema_editor.create_model(Document)
            schema_editor.create_model(Car)

        ContentType.objects.get_or_create(app_label='tests', model='person')
        ContentType.objects.get_or_create(app_label='tests', model='document')
        ContentType.objects.get_or_create(app_label='tests', model='car')

    def setUp(self):
        self.user = get_user_model().objects.create_user('foo', 'user@test.com', 'bar')
        self.user.is_staff = True
        self.user.save()

        self.group, created = Group.objects.get_or_create(name='New Group')

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'global_related_fields_exclude': (),
                                'related_filter_fields_exclude': {'tests.car': ('person', ), },
                                }
                       )
    def test_related_filter_fields_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(content_type=ContentType.objects.get_for_model(Person),
                                     filter_type=EXCLUDE_ID,
                                     filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        self.assertTrue(Car.objects.apply_user_object_filter(user=self.user).exists())

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': ('person'),
                                'global_related_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_global_fields_exclude(self):
        foo = Person(name='foo', user=None)
        foo.save()

        bar = Person(name='bar', user=self.user)
        bar.save()

        car1 = Car(name='bar', person=foo)
        car1.save()

        car2 = Car(name='bar', person=bar)
        car2.save()

        objectfilter2 = ObjectFilter(content_type=ContentType.objects.get_for_model(Person),
                                     filter_type=EXCLUDE_ID,
                                     filter_values="[2, 3]")
        objectfilter2.save()
        objectfilter2.users.add(self.user)

        objectfilter2._create_filter_dict(user=self.user)
        self.assertFalse('tests.car' in objectfilter2._filter_dict)
