# -*- coding: utf-8 -*-
from django.conf.urls import include, url

from .views import (UserListView, UserDetailView, UserUpdateView, UserDeleteView,
                    ObjectFilterListView)

urlpatterns = [
    url(r'^', include('dj_auth.urls')),
    url(r'^user/$', UserListView.as_view(), name='user-list'),
    url(r'^user/(?P<pk>[0-9]+)/$', UserDetailView.as_view(), name='user-detail'),
    url(r'^user/(?P<pk>[0-9]+)/update/$', UserUpdateView.as_view(), name='user-update'),
    url(r'^user/(?P<pk>[0-9]+)/delete/$', UserDeleteView.as_view(), name='user-delete'),
    url(r'^objectfilter/$', ObjectFilterListView.as_view(), name='objectfilter-list'),
]
