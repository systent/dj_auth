#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase, override_settings
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from dj_auth.models import ObjectFilter
from dj_auth.constants import FILTER_ID

from .forms import ObjectFilterForm


class test_UserForm(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user('foo', 'user@test.com', 'bar')
        self.user.is_staff = True
        self.user.save()

        self.group, created = Group.objects.get_or_create(name='New Group')

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_form(self):
        form = ObjectFilterForm(data={})
        self.assertFalse(form.is_valid())

    @override_settings(DJ_AUTH={'content_type_exclude': ('auth.user',),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'content_type_fields_include': {},
                                'content_type_fields_exclude': {},
                                }
                       )
    def test_form_content_type_exclude(self):
        form = ObjectFilterForm(data={})
        self.assertFalse(form.is_valid())

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': ('auth.user',),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_empty_form_content_type_include(self):
        form = ObjectFilterForm(data={})
        self.assertFalse(form.is_valid())

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': ('auth.user',),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_form_content_type_include(self):
        form = ObjectFilterForm(data={
            'content_type': ContentType.objects.get_for_model(get_user_model()).id,
            'filter_type': FILTER_ID,
            'filter_values': '[%s, ]' % self.group.id})
        self.assertFalse(form.is_valid())

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': ('auth.user',),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_form_filter_values_null_text_content_type_include(self):
        form = ObjectFilterForm(data={
            'content_type': ContentType.objects.get_for_model(get_user_model()).id,
            'filter_type': FILTER_ID,
            'filter_values': '[%s, ]' % self.group.id})
        self.assertFalse(form.is_valid())

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': ('auth.user',),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_form_update(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        form = ObjectFilterForm(instance=objectfilter)
        self.assertFalse(form.is_valid())
