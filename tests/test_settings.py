#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase

from dj_auth.settings import *


class test_ObjectFilterFieldWidget(TestCase):

    def test_DJ_AUTH(self):
        self.assertEquals(DJ_AUTH, {'content_type_exclude': (),
                                    'content_type_include': (),
                                    'global_fields_exclude': (),
                                    'global_related_fields_exclude': (),
                                    'related_filter_fields_exclude': {},
                                    })
