#!/usr/bin/env python
# -*- coding: utf-8 -*-
from django.test import TestCase, override_settings
from django.contrib.contenttypes.models import ContentType
try:
    from django.urls import reverse
except ImportError:
    from django.core.urlresolvers import reverse
from django.contrib.auth import get_user_model
from django.contrib.auth.models import Group

from dj_auth.models import ObjectFilter
from dj_auth.constants import FILTER_ID, EXCLUDE_ID


class ObjectFilterListMixinTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user('foo', 'user@test.com', 'bar')
        self.user.is_staff = True
        self.user.save()

        self.group, created = Group.objects.get_or_create(name='New Group')

    def test_m2m_tuple_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_m2m_tuple_exclude(self):
        objectexclude = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID,
            filter_values=[self.group.id, ])
        objectexclude.save()
        objectexclude.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_m2m_list_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_m2m_list_exclude(self):
        objectexclude = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[self.group.id, ])
        objectexclude.save()
        objectexclude.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_isnull_list_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_isnull_list_exclude(self):
        objectexclude = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[])
        objectexclude.save()
        objectexclude.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_m2m_not_tuple_list_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=self.group.id)
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        with self.assertRaises(ValueError):
            self.client.get(reverse('user-list'), follow=True)

    def test_fk_int_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(ObjectFilter), filter_type=FILTER_ID,
            filter_values=[ContentType.objects.get_for_model(get_user_model()).id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_fk_not_int_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(ObjectFilter),
            filter_type=FILTER_ID, filter_values="(1, )")
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        with self.assertRaises(ValueError):
            self.client.get(reverse('objectfilter-list'), follow=True)

    def test_wrong_filter_type(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(ObjectFilter),
            filter_type=2, filter_values=1)
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        with self.assertRaises(ValueError):
            self.client.get(reverse('objectfilter-list'), follow=True)

    def test_not_logged_in(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(ObjectFilter),
            filter_type=FILTER_ID, filter_values=1)
        objectfilter.save()
        objectfilter.users.add(self.user)
        response = self.client.get(reverse('objectfilter-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_m2m_extend_list_filter(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=FILTER_ID, filter_values=[6, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_m2m_extend_list_exclude(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[6, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)

    @override_settings(INSTALLED_APPS=["django.contrib.auth",
                                       "django.contrib.contenttypes",
                                       "django.contrib.sessions",
                                       "django.contrib.sites",
                                       "dj_auth",
                                       "tests"
                                       ],
                       )
    def test_m2m_extend_list_exclude_session(self):
        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        objectfilter = ObjectFilter(
            content_type=ContentType.objects.get_for_model(get_user_model()),
            filter_type=EXCLUDE_ID, filter_values=[6, ])
        objectfilter.save()
        objectfilter.users.add(self.user)

        # write in session
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)
        # read from session
        response = self.client.get(reverse('user-list'), follow=True)
        self.assertEquals(response.status_code, 200)


class ObjectFilterDetailMixinTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user('foo', 'user@test.com', 'bar')
        self.user.is_staff = True
        self.user.save()

        self.group, created = Group.objects.get_or_create(name='New Group')

    def test_allowed(self):
        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Group),
                                    filter_type=FILTER_ID,
                                    filter_values=[])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_not_allowed(self):
        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Group),
                                    filter_type=FILTER_ID,
                                    filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), follow=True)
        self.assertEquals(response.status_code, 403)

    def test_allowed_exclude(self):
        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Group),
                                    filter_type=EXCLUDE_ID,
                                    filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), follow=True)
        self.assertEquals(response.status_code, 200)

    def test_not_allowed_exclude(self):
        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Group),
                                    filter_type=EXCLUDE_ID,
                                    filter_values=[])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.client.login(username='foo', password='bar')
        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), follow=True)
        self.assertEquals(response.status_code, 403)

    def test_not_logged_in(self):
        objectfilter = ObjectFilter(content_type=ContentType.objects.get_for_model(Group),
                                    filter_type=FILTER_ID,
                                    filter_values=[self.group.id, ])
        objectfilter.save()
        objectfilter.users.add(self.user)
        self.user.groups.add(self.group)

        response = self.client.get(reverse('user-detail', kwargs={'pk': self.user.id}), follow=True)
        self.assertEquals(response.status_code, 200)


class ObjectFilterValuesWidgetViewTest(TestCase):

    def setUp(self):
        self.user = get_user_model().objects.create_user('foo', 'user@test.com', 'bar')
        self.user.is_staff = True
        self.user.save()

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_empty_DJ_AUTH(self):
        self.client.login(username='foo', password='bar')
        response = self.client.get(
            reverse(
                'loadvalues',
                kwargs={'content_type_id': ContentType.objects.get_for_model(get_user_model()).id}),
            follow=True)
        self.assertEquals(response.status_code, 200)

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': ('groups', ),
                                'related_filter_fields_exclude': {},
                                }
                       )
    def test_global_fields_exclude(self):
        self.client.login(username='foo', password='bar')
        response = self.client.get(
            reverse(
                'loadvalues',
                kwargs={'content_type_id': ContentType.objects.get_for_model(get_user_model()).id}),
            follow=True)
        self.assertEquals(response.status_code, 200)

    @override_settings(DJ_AUTH={'content_type_exclude': (),
                                'content_type_include': (),
                                'global_fields_exclude': (),
                                'related_filter_fields_exclude': {'auth.user': ('groups', ), },
                                }
                       )
    def test_content_type_fields_exclude(self):
        self.client.login(username='foo', password='bar')
        response = self.client.get(
            reverse(
                'loadvalues',
                kwargs={'content_type_id': ContentType.objects.get_for_model(get_user_model()).id}),
            follow=True)
        self.assertEquals(response.status_code, 200)
