# -*- coding: utf-8 -*-
from django import forms
from django.contrib.auth import get_user_model

from dj_auth.forms import ObjectFilterFormMixin


class UserUpdateForm(forms.ModelForm):

    class Meta:
        model = get_user_model()
        fields = "__all__"


class ObjectFilterForm(ObjectFilterFormMixin, forms.ModelForm):
    pass
