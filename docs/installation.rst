============
Installation
============

At the command line::

    $ easy_install dj_auth

Or, if you have virtualenvwrapper installed::

    $ mkvirtualenv dj_auth
    $ pip install dj_auth
